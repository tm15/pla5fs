package ga.manuelgarciacr.tripmemories.model;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

@SuppressWarnings({"unchecked"})
public class TripRepository {
    private MutableLiveData<Trip> mutableLiveDataTrip = new MutableLiveData<>();
    private static TripRepository mTripRepository;
    private File mFilesDir;

    private CollectionReference mCollectionReference;


    public synchronized static TripRepository getInstance(Application application) {
        if (mTripRepository == null)
            mTripRepository = new TripRepository(application);
        return mTripRepository;
    }

    private TripRepository(Application application) {
        mFilesDir = application.getApplicationContext().getFilesDir();

        FirebaseFirestore.setLoggingEnabled(true);
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        mCollectionReference = db.collection("cifotest");
    }

    public File getPhotoFile(Trip trip) {
        return new File(mFilesDir, trip.getPhoto());
    }

    public void allTrips(Request request, LiveData<Request> liveData){
        mCollectionReference
            .get()
            .addOnCompleteListener(task -> {
                if (task.isSuccessful() && task.getResult() != null) {
                    List<Trip> lst = new ArrayList<>();
                    Trip t;
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", new Locale("es", "ES"));
                    try {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            //noinspection ConstantConditions
                            t = new Trip(
                                    document.getString("name"),
                                    document.getString("country"),
                                    sdf.parse(document.getString("date")),
                                    document.getString("comp"),
                                    document.getString("photo"),
                                    "0",
                                    document.getString("phone"), 0, 0);
                            t.setUuid(UUID.fromString(document.getString("uuid")));
                            lst.add(t);
                        }
                    } catch (ParseException | NullPointerException e) {
                        e.printStackTrace();
                    }
                    Collections.sort(lst);
                    ((MutableLiveData) liveData).setValue(request.getRequestResponse(1, lst));
                } else {
                    ((MutableLiveData)liveData).setValue(request.getRequestResponse(-1, null));
                }
            });
        ((MutableLiveData)liveData).setValue(request);
    }

    public void insertTrip(Request request, LiveData<Request> liveData) {
        Map<String, Object> data = new HashMap<>();
        data.put("name", ((Trip)request.getT()).getName());
        data.put("country", ((Trip)request.getT()).getCountry());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", new Locale("es", "ES"));
        data.put("date", sdf.format(((Trip)request.getT()).getDate()));
        data.put("comp", ((Trip)request.getT()).getCompanion());
        data.put("photo", ((Trip)request.getT()).getPhoto());
        data.put("deleted", "0");
        data.put("phone", ((Trip)request.getT()).getPhone());
        data.put("uuid", ((Trip)request.getT()).getUuid().toString());

        mCollectionReference
            .whereEqualTo("uuid", ((Trip)request.getT()).getUuid().toString())
            .get()
            .addOnSuccessListener(queryDocumentSnapshots -> {
                boolean exist = !queryDocumentSnapshots.getDocuments().isEmpty();
                if (exist) {
                    queryDocumentSnapshots.getDocuments().get(0).getReference().set(data)
                            .addOnCompleteListener(task -> {
                                if (task.isSuccessful())
                                    ((MutableLiveData) liveData).setValue(request.getRequestResponse(1, request.getT()));
                                else {
                                    ((MutableLiveData) liveData).setValue(request.getRequestResponse(-1, null));
                                    Log.e("ERROR TRIPPING", "UPDATETRIP " + Objects.requireNonNull((Trip) request.getT()).toString());
                                }
                            });
                } else {
                    mCollectionReference.add(data).addOnCompleteListener(task -> {
                        if (task.isSuccessful())
                            ((MutableLiveData) liveData).setValue(request.getRequestResponse(1, request.getT()));
                        else {
                            ((MutableLiveData) liveData).setValue(request.getRequestResponse(-1, null));
                            Log.e("ERROR TRIPPING", "INSERTTRIP " + Objects.requireNonNull((Trip) request.getT()).toString());
                        }
                    });
                }
            })
            .addOnFailureListener(e -> {
                ((MutableLiveData) liveData).setValue(request.getRequestResponse(-1, null));
                Log.e("ERROR TRIPPING", "UPDATE/INSERTTRIP " + Objects.requireNonNull((Trip)request.getT()).toString());
            });
        ((MutableLiveData)liveData).setValue(request);
    }

    public void deleteTrip(Request request, LiveData<Request> liveData) {
        AtomicReference<List<DocumentSnapshot>> doc = new AtomicReference<>();
        mCollectionReference
            .whereEqualTo("uuid", ((Trip)request.getT()).getUuid().toString())
            .get()
            .addOnCompleteListener(task -> {
                if (task.isSuccessful() && task.getResult() != null) {
                    doc.set(task.getResult().getDocuments());
                    if(!doc.get().isEmpty()){
                        doc.get().get(0).getReference().delete().addOnCompleteListener(deleteTask -> {
                            if (deleteTask.isSuccessful()) {
                                ((MutableLiveData)liveData).setValue(request.getRequestResponse(1, null));
                            } else {
                                ((MutableLiveData)liveData).setValue(request.getRequestResponse(-1, null));
                                Log.e("ERROR TRIPPING", "DELETETRIP " + Objects.requireNonNull((Trip)request.getT()).toString());
                            }
                        });
                    }else{
                        ((MutableLiveData)liveData).setValue(request.getRequestResponse(-1, null));
                        Log.e("ERROR TRIPPING", "DELETETRIP " + Objects.requireNonNull((Trip)request.getT()).toString());
                    }
                } else {
                    ((MutableLiveData)liveData).setValue(request.getRequestResponse(-1, null));
                    Log.e("ERROR TRIPPING", "DELETETRIP " + Objects.requireNonNull((Trip)request.getT()).toString());
                }
            });
        ((MutableLiveData)liveData).setValue(request);
    }

    public MutableLiveData<Trip> getTripById(UUID uuid) {
        mCollectionReference
            .whereEqualTo("uuid", uuid.toString())
            .get()
            .addOnCompleteListener(task -> {
                if (task.isSuccessful() && task.getResult() != null) {
                    if(task.getResult().isEmpty())
                        mutableLiveDataTrip.setValue(null);
                    else {
                        DocumentSnapshot doc = task.getResult().getDocuments().get(0);
                        Trip t = null;
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", new Locale("es", "ES"));
                        try {
                            //noinspection ConstantConditions
                            t = new Trip(
                                doc.getString("name"),
                                doc.getString("country"),
                                sdf.parse(doc.getString("date")),
                                doc.getString("comp"),
                                doc.getString("photo"),
                                "0",
                                doc.getString("phone"), 0, 0);
                            t.setUuid(UUID.fromString(doc.getString("uuid")));
                        } catch (ParseException | NullPointerException e) {
                            e.printStackTrace();
                        }
                        mutableLiveDataTrip.setValue(t);
                    }
                } else {
                    mutableLiveDataTrip.setValue(null);
                    Log.e("ERROR TRIPPING", "GETTRIPBYID " + Objects.requireNonNull(uuid).toString());
                }
            });
        return mutableLiveDataTrip;
    }
}
