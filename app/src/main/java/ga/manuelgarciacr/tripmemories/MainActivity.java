package ga.manuelgarciacr.tripmemories;

import android.Manifest;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.firebase.ui.auth.AuthUI;
import com.google.android.material.navigation.NavigationView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.UUID;

import ga.manuelgarciacr.tripmemories.ui.home.HomeFragment;

import static ga.manuelgarciacr.tripmemories.ui.detail.DetailFragment.ARG_TRIP_ID;

public class MainActivity extends AppCompatActivity implements HomeFragment.Callbacks {

    private static final int MY_REQUEST_CODE = 1;
    private static final String STARTED_KEY = "started_key";
    static final String LOGOUT_EXTRA = "logout";
    private AppBarConfiguration mAppBarConfiguration;
    private NavController mNavController;
    boolean doubleBackToExitPressedOnce = false;
    //boolean mStarted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_map, R.id.nav_about)
                .setDrawerLayout(drawer)
                .build();
        mNavController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, mNavController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, mNavController);

        navigationView.setNavigationItemSelectedListener(menuItem -> {
            if (menuItem.getItemId() == R.id.logout){
                AuthUI.getInstance().signOut(MainActivity.this);
                this.finishAffinity();
            }
            NavigationUI.onNavDestinationSelected(menuItem, mNavController);
            drawer.closeDrawer(GravityCompat.START);
            return true;
        });

        //if(mStarted)
        //    return;
        //mStarted = true;
        /*
        ArrayList<String> permissions = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED)
            permissions.add(Manifest.permission.READ_CONTACTS);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED)
            permissions.add(Manifest.permission.CALL_PHONE);
        if(permissions.size() > 0) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
            ActivityCompat.requestPermissions(this,
                    permissions.toArray(new String[0]),
                    MY_REQUEST_CODE);
        }

         */
    }

    @Override
    protected void onSaveInstanceState(@NotNull Bundle state) {
        super.onSaveInstanceState(state);
        //state.putBoolean(STARTED_KEY, mStarted);
    }
/*
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[]
            grantResults) {
        if (requestCode == MY_REQUEST_CODE) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
            StringBuilder msgOk = new StringBuilder();
            StringBuilder msgKo = new StringBuilder();
            for(int i = 0; i < permissions.length; i++){
                if(grantResults[i] == PackageManager.PERMISSION_GRANTED){
                    if(msgOk.toString().equals(""))
                        msgOk = new StringBuilder("Ara podràs ");
                    else
                        msgOk.append(", ");
                    msgOk.append(getPermissionString(permissions[i]));
                }else{
                    if(msgKo.toString().equals(""))
                        msgKo = new StringBuilder("Assegura't de donar permisos a l'aplicació per ");
                    else
                        msgKo.append(", ");
                    msgKo.append(getPermissionString(permissions[i]));
                }
            }
            msgOk = new StringBuilder(msgOk.toString().replaceFirst("(?s)(.*),", "$1 i"));
            msgKo = new StringBuilder(msgKo.toString().replaceFirst("(?s)(.*),", "$1 i"));
            if(!msgOk.toString().equals(""))
                msgOk.append(".");
            if(!msgKo.toString().equals("")) {
                if(!msgOk.toString().equals(""))
                    msgOk.append(" ");
                msgOk.append(msgKo).append(".");
            }
            if(msgKo.toString().equals(""))
                Toast.makeText(this, msgOk.toString(),
                    Toast.LENGTH_LONG).show();
            else
                new AlertDialog.Builder(this)
                    .setTitle("Permissions")
                    .setMessage(msgOk.toString())
                    .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                    })
                    //.setNegativeButton(android.R.string.no, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }
    }

    private String getPermissionString(String permission){
        switch(permission){
            case Manifest.permission.ACCESS_FINE_LOCATION:
                return "localitzarte al mapa";
            case Manifest.permission.CALL_PHONE:
                return "trucar als teus contactes";
            default:
                return "accedir als teus contactes";
        }
    }
*/
    @Override
    public boolean onSupportNavigateUp() {
        return NavigationUI.navigateUp(mNavController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
    /****************************************************************
     *
     *     Callbacks
     */

    @Override
    public void onTripSelected(UUID trip) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_TRIP_ID, trip);
        NavDestination id;
        if ((id = mNavController.getCurrentDestination()) != null &&
                id.getId() == R.id.nav_home)
            mNavController.navigate(R.id.action_nav_home_to_nav_detail, args);
    }

    /*
     *
     *     Callbacks
     ****************************************************************/

    @Override
    public void onBackPressed() {
        NavDestination id;
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else if((id = mNavController.getCurrentDestination()) != null && id.getId() == R.id.nav_home) {
            if (doubleBackToExitPressedOnce) {
                this.finishAffinity();
            }
            doubleBackToExitPressedOnce = true;
            Toast.makeText(this, R.string.torneu_a_premer, Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
        }else{
            mNavController.navigate(R.id.nav_home);
        }
    }
}
