package ga.manuelgarciacr.tripmemories;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Collections;

import static ga.manuelgarciacr.tripmemories.MainActivity.LOGOUT_EXTRA;

public class SplashActivity extends AppCompatActivity {
    private static final int MY_SIGN_IN = 1;
    private static final int MY_REQUEST_CODE = 1;
    ImageView mImvSplash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseFirestore.setLoggingEnabled(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        if(intent.getStringExtra(LOGOUT_EXTRA) != null || shouldStartSignIn())
            startSignIn();
        else
            checkPermissions();
    }

    private boolean shouldStartSignIn() {
        return (FirebaseAuth.getInstance().getCurrentUser() == null);
    }

    private void startSignIn() {
        Intent intent = AuthUI.getInstance().createSignInIntentBuilder()
                .setAvailableProviders(Collections.singletonList(
                        new AuthUI.IdpConfig.EmailBuilder().build()))
                .setIsSmartLockEnabled(!BuildConfig.DEBUG)
                .setTheme(R.style.FirebaseUI)
                .setLogo(R.drawable.ic_launcher_foreground)
                .build();
        startActivityForResult(intent, MY_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_SIGN_IN) {
            if (resultCode != RESULT_OK && shouldStartSignIn())
                startSignIn();
            else
                checkPermissions();
        }
    }

    private void checkPermissions() {
        ArrayList<String> permissions = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED)
            permissions.add(Manifest.permission.READ_CONTACTS);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED)
            permissions.add(Manifest.permission.CALL_PHONE);
        if(permissions.size() > 0) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);
            ActivityCompat.requestPermissions(this,
                    permissions.toArray(new String[0]),
                    MY_REQUEST_CODE);
        }else{
            startMain();
        }
    }

    @SuppressWarnings("StringConcatenationInLoop")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[]
            grantResults) {
        if (requestCode == MY_REQUEST_CODE) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
            String msgOk = "";
            String msgKo = "";
            for(int i = 0; i < permissions.length; i++){
                if(grantResults[i] == PackageManager.PERMISSION_GRANTED){
                    if(msgOk.equals(""))
                        msgOk = "Ara podràs ";
                    else
                        msgOk += ", ";
                    msgOk += getPermissionString(permissions[i]);
                }else{
                    if(msgKo.equals(""))
                        msgKo = "Assegura't de donar permisos a l'aplicació per ";
                    else
                        msgKo += ", ";
                    msgKo += getPermissionString(permissions[i]);
                }
            }
            msgOk = msgOk.replaceFirst("(?s)(.*),", "$1 i");
            msgKo = msgKo.replaceFirst("(?s)(.*),", "$1 i");
            if(!msgOk.equals(""))
                msgOk += ".";
            if(!msgKo.equals("")) {
                if(!msgOk.equals(""))
                    msgOk += " ";
                msgOk += msgKo + ".";
            }
            if(msgKo.equals("") && !msgOk.equals("")){
                Toast.makeText(this, msgOk,
                        Toast.LENGTH_LONG).show();
                startMain();
            }else if(!msgOk.equals("")){
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Permissions");
                builder.setMessage(msgOk);
                builder.setPositiveButton(android.R.string.yes, (dialog, which) -> {
                });
                builder.setIcon(android.R.drawable.ic_dialog_alert);
                builder.setOnDismissListener(dialog -> startMain());
                builder.show();
            }
        }
    }

    private String getPermissionString(String permission){
        switch(permission){
            case Manifest.permission.ACCESS_FINE_LOCATION:
                return "localitzarte al mapa";
            case Manifest.permission.CALL_PHONE:
                return "trucar als teus contactes";
            default:
                return "accedir als teus contactes";
        }
    }

    private void startMain(){
        setContentView(R.layout.activity_splash);
        mImvSplash=findViewById(R.id.imvSplash);
        Animation an= AnimationUtils.loadAnimation(this,R.anim.scale);
        mImvSplash.startAnimation(an);
        int SPLASH_TIME_OUT = 1900;
        new Handler().postDelayed(() -> {
            Intent i = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        }, SPLASH_TIME_OUT);
    }
}
