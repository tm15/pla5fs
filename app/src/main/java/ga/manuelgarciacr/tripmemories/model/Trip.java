package ga.manuelgarciacr.tripmemories.model;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

import java.util.Date;
import java.util.UUID;

public class Trip implements Comparable<Trip>, Parcelable {
    @NonNull
    @SerializedName("uuid")
    @Expose
    private UUID uuid = java.util.UUID.randomUUID();
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("date")
    @Expose
    private Date date;
    @SerializedName("comp")
    @Expose
    private String companion;
    @SerializedName("photo")
    @Expose
    private String photo;
    @SerializedName("deleted")
    @Expose
    private String deleted;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("latitude")
    @Expose
    private double latitude;
    @SerializedName("longitude")
    @Expose
    private double longitude;

    private Bitmap bitmap;

    private String email;

    public Trip(String name,
                String country,
                Date date,
                String companion,
                String photo,
                String deleted,
                String phone,
                double latitude,
                double longitude) {
        super();
        this.setName(name);
        this.setCountry(country);
        this.setDate(date);
        this.setCompanion(companion);
        this.setPhoto(photo);
        this.setDeleted(deleted);
        this.setPhone(phone);
        this.setLatitude(latitude);
        this.setLongitude(longitude);
    }
    public Trip(){

    }

    public Trip(String name, String country) {
        this(name, country, new Date(), "", "", "0", "", 0, 0);
    }

    @NotNull
    public UUID getUuid() {
        return uuid;
    }

    void setUuid(@NotNull UUID uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if(name == null)
            this.name = "";
        else
            this.name = name.trim();
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        if(country == null)
            this.country = "";
        else
            this.country = country.trim();
    }

    @NonNull
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCompanion() {
        return companion;
    }

    public void setCompanion(String companion) {
        if(companion == null)
            this.companion = "";
        else
            this.companion = companion.trim();
    }

    public String getPhoto() {
        if(this.photo.equals(""))
            setPhoto("IMG_" + getUuid() + ".jpg");
        return this.photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public Trip getReplicate(){
        return new Trip(getName(), getCountry(), getDate(), getCompanion(), getPhoto(), getDeleted(), getPhone(), getLatitude(), getLongitude());
    }

    @NonNull
    @Override
    public String toString() {
        return "[" + getDate().toString() + ": " + getName() + ", " + getCountry() + ", " + getCompanion() + ", " + getUuid() + "]";
    }

    @Override
    public int compareTo(Trip o) {
        return (this.getName().compareTo(o.getName()));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getUuid().toString());
        dest.writeString(getName());
        dest.writeString(getCountry());
        //noinspection ConstantConditions
        dest.writeLong(getDate() != null ? date.getTime() : -1);
        dest.writeString(getCompanion());
        dest.writeString(getPhoto());
        dest.writeString(getDeleted());
        dest.writeString(getPhone());
        dest.writeDouble(getLatitude());
        dest.writeDouble(getLongitude());
        dest.writeString(getEmail());
        dest.writeParcelable(getBitmap(), flags);
    }

    private Trip(Parcel in){
        setUuid(UUID.fromString(in.readString()));
        setName(in.readString());
        setCountry(in.readString());
        long tmpDate = in.readLong();
        setDate(tmpDate == -1 ? null : new Date(tmpDate));
        setCompanion(in.readString());
        setPhoto(in.readString());
        setDeleted(in.readString());
        setPhone(in.readString());
        setLatitude(in.readDouble());
        setLongitude(in.readDouble());
        setEmail(in.readString());
        setBitmap(in.readParcelable(getClass().getClassLoader()));
    }

    public static final Parcelable.Creator<Trip> CREATOR
            = new Parcelable.Creator<Trip>() {
        public Trip createFromParcel(Parcel in) {
            return new Trip(in);
        }

        public Trip[] newArray(int size) {
            return new Trip[size];
        }
    };

}
