package ga.manuelgarciacr.tripmemories.services;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class AddressesResultReceiver extends ResultReceiver {
    private AddressesReceiver mReceiver;

    public AddressesResultReceiver(Handler handler) {
        super(handler);
    }

    public interface AddressesReceiver {
        void onAddressesReceiveResult(int resultCode, Bundle resultData);

    }

    public void setReceiver(AddressesReceiver receiver) {
        mReceiver = receiver;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (mReceiver != null) {
            mReceiver.onAddressesReceiveResult(resultCode, resultData);
        }
    }
}
