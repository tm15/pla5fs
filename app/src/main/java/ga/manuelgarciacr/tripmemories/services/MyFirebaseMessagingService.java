package ga.manuelgarciacr.tripmemories.services;

import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.jetbrains.annotations.NotNull;

import static ga.manuelgarciacr.tripmemories.util.utils.log;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onNewToken(@NotNull String token) {
        log("ONNEWTOKEN", token);
    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Task<InstanceIdResult> task = FirebaseInstanceId.getInstance().getInstanceId();
        task.addOnSuccessListener(instanceIdResult -> log("TOKEN", instanceIdResult.getToken()));
        log("ONMESSAGERECEIVED", remoteMessage.getMessageId()
            , String.valueOf(remoteMessage.getData()));
        if (remoteMessage.getData().size() > 0) {
            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(() -> Toast.makeText(getApplicationContext(),
                    remoteMessage.getData().get("missatge"), Toast.LENGTH_SHORT).show());
        }
    }
}
