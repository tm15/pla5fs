package ga.manuelgarciacr.tripmemories.ui.home;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

import ga.manuelgarciacr.tripmemories.R;
import ga.manuelgarciacr.tripmemories.model.RequestObserver;
import ga.manuelgarciacr.tripmemories.model.Trip;
import ga.manuelgarciacr.tripmemories.ui.viewmodel.ViewModel;
import ga.manuelgarciacr.tripmemories.util.Pgb;
import ga.manuelgarciacr.tripmemories.util.PictureUtils;

public class HomeFragment extends Fragment {
    private static final String STARTED_KEY = "started_key";
    private Callbacks mCallbacks = null;
    private View mView;
    private ViewModel mViewModel;
    private TripListAdapter mAdapter;
    private RequestObserver mRequestObserver;
    private boolean mStarted;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return mView = inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState != null)
            mStarted = savedInstanceState.getBoolean(STARTED_KEY, false);

        mAdapter = new TripListAdapter(getContext());
        RecyclerView recyclerView = mView.findViewById(R.id.rcvHomeFragment);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        mViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(ViewModel.class);

        ItemTouchHelper itemTouchHelper = new
                ItemTouchHelper(new SwipeToDeleteCallback(mAdapter));
        itemTouchHelper.attachToRecyclerView(recyclerView);

        Pgb mPgb = new Pgb(mView.findViewById(R.id.pgbHome), mView.findViewById(R.id.txvPgbHome));
        mRequestObserver = new RequestObserver(mPgb, mView);
        if(!mStarted || mViewModel.getAllTrips().getValue() == null
            || mViewModel.getAllTrips().getValue().getT() == null
            || mViewModel.getAllTrips().getValue().getSt() <= 0) {
            listAll();
            mStarted = true;
        }else //noinspection unchecked
            mAdapter.setTrips((List<Trip>) mViewModel.getAllTrips().getValue().getT());

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(@NotNull Bundle state) {
        state.putBoolean(STARTED_KEY, mStarted);
        super.onSaveInstanceState(state);
    }

    /****************************************************************
     *
     *     Callbacks
    */

    public interface Callbacks {
        void onTripSelected(UUID trip);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        mCallbacks = (Callbacks) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    /*
     *
     *     Callbacks
     ****************************************************************/

    /****************************************************************
     *
     *     Toolbar menu
     */

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NotNull Menu menu, @NotNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.home_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        item.setEnabled(false);
        if (item.getItemId() == R.id.new_trip) {
            Trip trip = new Trip("", "");
            UUID uuid = mViewModel.insertTrip(trip);
            mRequestObserver.<Trip>addObserver(mViewModel.getInsertResult(), getViewLifecycleOwner(),
                    "Inserting...","Insert trip failed", uuid, (request) -> mCallbacks.onTripSelected((request.getT()).getUuid()));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        mRequestObserver.<Trip>addObserver(mViewModel.getDeleteResult(), getViewLifecycleOwner(),
                "Deleting...","Trip deletion failed", null, (request) -> listAll());
        mRequestObserver.<Trip>addObserver(mViewModel.getUpdateResult(), getViewLifecycleOwner(),
                "Saving...","Update/insert failed", null,  (request) -> listAll());
    }

    private void listAll(){
        mViewModel.allTrips();
        mRequestObserver.<List<Trip>>addObserver(mViewModel.getAllTrips(), getViewLifecycleOwner(),
                "Loading...","Load trips failed", null, (request) -> mAdapter.setTrips(request.getT()));
    }

    public class TripListAdapter extends RecyclerView.Adapter<TripListAdapter.TripViewHolder>{
        private final LayoutInflater mInflater;
        private List<Trip> mAllTrips;
        private Trip mRecentlyDeletedItem;
        private int mRecentlyDeletedItemPosition;
        private Bitmap mDefaultBmp;

        class TripViewHolder extends RecyclerView.ViewHolder{
            private final ImageView tripImage;
            private final TextView tripCountry, tripName;
            private final CardView tripCardView;

            TripViewHolder(@NonNull View itemView) {
                super(itemView);
                tripImage = itemView.findViewById(R.id.imvItem);
                tripCountry = itemView.findViewById(R.id.txvItemCountry);
                tripName = itemView.findViewById(R.id.txvItemName);
                tripCardView = itemView.findViewById(R.id.trip_card_view);
            }
        }

        TripListAdapter(Context context) {
            mInflater = LayoutInflater.from(context);
            Drawable drw = getResources().getDrawable(R.mipmap.ic_logo);
            mDefaultBmp = Bitmap.createBitmap(drw.getIntrinsicWidth(), drw.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(mDefaultBmp);
            drw.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drw.draw(canvas);
        }

        void setTrips(List<Trip> trips) {
            mAllTrips = trips;
            for(Trip trip: trips)
                trip.setBitmap(PictureUtils.getScaledBitmapFromFile(
                    mViewModel.getPhoto(trip), getActivity(), .2f));
            notifyDataSetChanged();
        }

        @NonNull
        @Override
        public TripListAdapter.TripViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = mInflater.inflate(R.layout.fragment_home_rcv_item, parent, false);
            return new TripListAdapter.TripViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull TripListAdapter.TripViewHolder holder, int position) {
            final Trip current = mAllTrips.get(position);
            holder.tripCountry.setText(current.getCountry());
            holder.tripName.setText(current.getName());

            if(current.getBitmap() == null) {
                holder.tripImage.setImageBitmap(mDefaultBmp);
                holder.tripImage.setAlpha(.5f);
            }else {
                holder.tripImage.setImageBitmap(current.getBitmap());
                holder.tripImage.setAlpha(1f);
            }
            holder.tripCardView.setOnClickListener(view -> mCallbacks.onTripSelected(current.getUuid()));
        }

        @Override
        public int getItemCount() {
            if (mAllTrips != null)
                return mAllTrips.size();
            else return 0;
        }

        void deleteItem(int position) {
            mRecentlyDeletedItem = mAllTrips.get(position);
            mRecentlyDeletedItemPosition = position;
            mAllTrips.remove(position);
            mViewModel.deleteTrip(mRecentlyDeletedItem);
            notifyItemRemoved(position);
            showUndoSnackbar();
        }

        private void undoDelete() {
            mAllTrips.add(mRecentlyDeletedItemPosition,
                    mRecentlyDeletedItem);
            UUID uuid = mViewModel.insertTrip(mRecentlyDeletedItem.getReplicate());
            mRequestObserver.<Trip>addObserver(mViewModel.getInsertResult(), getViewLifecycleOwner(),
                    "Inserting...","Insertion failed", uuid, (request) -> listAll());
            notifyItemInserted(mRecentlyDeletedItemPosition);
        }

        private void showUndoSnackbar() {
            Snackbar snackbar = Snackbar.make(mView, R.string.one_trip_deleted,
                    Snackbar.LENGTH_LONG);
            snackbar.setAction(R.string.snack_bar_undo, v -> undoDelete());
            snackbar.show();
        }

    }

    public class SwipeToDeleteCallback extends ItemTouchHelper.SimpleCallback {
        private TripListAdapter mAdapter;
        private Drawable icon;
        private final ColorDrawable background;

        SwipeToDeleteCallback(TripListAdapter adapter) {
            super(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
            mAdapter = adapter;
            icon = ContextCompat.getDrawable(Objects.requireNonNull(getContext()),
                    R.drawable.ic_delete_sweep_white_24dp);
            background = new ColorDrawable(Color.RED);
        }

        @Override
        public void onChildDraw(@NotNull Canvas c, @NotNull RecyclerView recyclerView, @NotNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            super.onChildDraw(c, Objects.requireNonNull(recyclerView), viewHolder, dX,
                    dY, actionState, isCurrentlyActive);
            View itemView = viewHolder.itemView;
            int backgroundCornerOffset = 0;

            // Only Background
            /*
            if (dX > 0) { // Swiping to the right
                background.setBounds(itemView.getLeft(), itemView.getTop(),
                        itemView.getLeft() + ((int) dX) + backgroundCornerOffset,
                        itemView.getBottom());

            } else if (dX < 0) { // Swiping to the left
                background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset,
                        itemView.getTop(), itemView.getRight(), itemView.getBottom());
            } else { // view is unSwiped
                background.setBounds(0, 0, 0, 0);
            }
            background.draw(c);
            */

            // Icon
            int iconMargin = (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
            int iconTop = itemView.getTop() + (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
            int iconBottom = iconTop + icon.getIntrinsicHeight();

            if (dX > 0) { // Swiping to the right
                int iconLeft = itemView.getLeft() + iconMargin;
                int iconRight = itemView.getLeft() + iconMargin + icon.getIntrinsicWidth();
                icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                background.setBounds(itemView.getLeft(), itemView.getTop(),
                        itemView.getLeft() + ((int) dX) + backgroundCornerOffset,
                        itemView.getBottom());
            } else if (dX < 0) { // Swiping to the left
                int iconLeft = itemView.getRight() - iconMargin - icon.getIntrinsicWidth();
                int iconRight = itemView.getRight() - iconMargin;
                icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset,
                        itemView.getTop(), itemView.getRight(), itemView.getBottom());
            } else { // view is unSwiped
                background.setBounds(0, 0, 0, 0);
            }

            background.draw(c);
            icon.draw(c);
        }

        /*
        @Override
        public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            if (mPgb.getVisibility() == ProgressBar.VISIBLE) return 0;
            return super.getSwipeDirs(recyclerView, viewHolder);
        }
        */

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            int position = viewHolder.getAdapterPosition();
            mAdapter.deleteItem(position);
        }
    }
}

