package ga.manuelgarciacr.tripmemories.ui.map;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import ga.manuelgarciacr.tripmemories.R;
import ga.manuelgarciacr.tripmemories.model.RequestObserver;
import ga.manuelgarciacr.tripmemories.model.Trip;
import ga.manuelgarciacr.tripmemories.services.AddressesResultReceiver;
import ga.manuelgarciacr.tripmemories.services.FetchAddressesIntentService;
import ga.manuelgarciacr.tripmemories.ui.viewmodel.ViewModel;
import ga.manuelgarciacr.tripmemories.util.Pgb;

import static ga.manuelgarciacr.tripmemories.services.FetchAddressesIntentService.Constants.RESULT_DATA_KEY;
import static ga.manuelgarciacr.tripmemories.services.FetchAddressesIntentService.Constants.RESULT_ORIGIN_KEY;
import static ga.manuelgarciacr.tripmemories.services.FetchAddressesIntentService.Constants.SUCCESS_RESULT;

public class MapFragment extends Fragment
        implements OnMapReadyCallback, AddressesResultReceiver.AddressesReceiver {
    private GoogleMap mMap;
    private AddressesResultReceiver mAddressesResultReceiver;
    private Pgb mPgb;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAddressesResultReceiver = new AddressesResultReceiver(new Handler());
        mAddressesResultReceiver.setReceiver(this);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_map, container, false);
        ViewModel mViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(ViewModel.class);
        mPgb = new Pgb(mView.findViewById(R.id.pgbMap), mView.findViewById(R.id.txvPgbMap));
        RequestObserver mRequestObserver = new RequestObserver(mPgb, mView);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);

        mViewModel.allTrips();
        mRequestObserver.<List<Trip>>addObserver(mViewModel.getAllTrips(), getViewLifecycleOwner(),
                "Loading...","Load trips failed", null, (request) -> {
            for(Trip trip: request.getT())
                startLocationIntent(trip.getName() + ", " + trip.getCountry());
        });

        return mView;
    }

    private void startLocationIntent(String address){
        if (!Geocoder.isPresent()) {
            Toast.makeText(getContext(),
                    R.string.no_geocoder_available,
                    Toast.LENGTH_LONG).show();
            return;
        }
        mPgb.add("Loading addresses...");
        Intent intent = new Intent(getContext(), FetchAddressesIntentService.class);
        intent.putExtra(FetchAddressesIntentService.Constants.RECEIVER, mAddressesResultReceiver);
        intent.putExtra(FetchAddressesIntentService.Constants.ADDRESS_EXTRA, address);
        intent.putExtra(FetchAddressesIntentService.Constants.COUNT_EXTRA, 3);
        Objects.requireNonNull(getContext()).startService(intent);
    }

    @Override
    public void onAddressesReceiveResult(int resultCode, Bundle resultData) {
        mPgb.remove();
        if(resultCode == SUCCESS_RESULT && resultData != null) {
            ArrayList<Address> addresses = resultData.getParcelableArrayList(RESULT_DATA_KEY);
            assert addresses != null;
            for (int i = 0; i < addresses.size(); i++) {
                ArrayList<String> addressFragments = new ArrayList<>();
                for (int j = 0; j <= addresses.get(i).getMaxAddressLineIndex(); j++) {
                    addressFragments.add(addresses.get(i).getAddressLine(j));
                }
                LatLng latLng = new LatLng(addresses.get(i).getLatitude(), addresses.get(i).getLongitude());
                String title = resultData.getString(RESULT_ORIGIN_KEY);
                String snippet = TextUtils.join(Objects.requireNonNull(System.getProperty("line.separator")), addressFragments);
                mMap.addMarker(new MarkerOptions().position(latLng)
                        .title(title)
                        .snippet(snippet).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getContext()),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        } else {
            Toast.makeText(getContext(), "Assegura't de donar permisos a l'aplicació per " +
                "acceir a la teva localització", Toast.LENGTH_LONG).show();
        }
    }

}