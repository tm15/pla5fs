package ga.manuelgarciacr.tripmemories.ui.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import java.io.File;
import java.util.UUID;

import ga.manuelgarciacr.tripmemories.model.Request;
import ga.manuelgarciacr.tripmemories.model.Trip;
import ga.manuelgarciacr.tripmemories.model.TripRepository;

public class ViewModel extends AndroidViewModel {
    private final TripRepository mTripRepository;

    private LiveData<Request> mAllTrips = new MutableLiveData<>();
    private LiveData<Request> mInsertResult = new MutableLiveData<>();
    private LiveData<Request> mUpdateResult = new MutableLiveData<>();
    private LiveData<Request> mDeleteResult = new MutableLiveData<>();

    private MutableLiveData<UUID> tripIdLiveData = new MutableLiveData<>();
    public LiveData<Trip> tripLiveData;

    public ViewModel(@NonNull Application application) {
        super(application);
        mTripRepository = TripRepository.getInstance(application);
        tripLiveData = Transformations.switchMap(tripIdLiveData, mTripRepository::getTripById);
    }

    public void allTrips() {
        mTripRepository.allTrips(new Request("list"), mAllTrips);
    }

    public UUID insertTrip(Trip trip) {
        Request<Trip> request = new Request<>(trip, "insert");
        mTripRepository.insertTrip(request, mInsertResult);
        return request.getUuid();
    }

    public void updateTrip(Trip trip) {
        mTripRepository.insertTrip(new Request<>(trip, "update"), mUpdateResult);
    }

    public void deleteTrip(Trip trip) {
        mTripRepository.deleteTrip(new Request<>(trip, "delete"), mDeleteResult);
    }

    public LiveData<Request> getAllTrips() {
        return mAllTrips;
    }

    public LiveData<Request> getInsertResult() {
        return mInsertResult;
    }

    public LiveData<Request> getUpdateResult() {
        return mUpdateResult;
    }

    public LiveData<Request> getDeleteResult() {
        return mDeleteResult;
    }

    public void loadUUID(UUID mTripId) {
        tripIdLiveData.setValue(mTripId);
    }

    public File getPhoto(Trip trip) {
        return mTripRepository.getPhotoFile(trip);
    }
}