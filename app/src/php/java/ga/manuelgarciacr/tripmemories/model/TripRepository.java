package ga.manuelgarciacr.tripmemories.model;

import android.app.Application;
import android.os.Handler;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressWarnings({"unchecked"})
public class TripRepository {
    private final RestApiService apiService;
    private MutableLiveData<Trip> mutableLiveDataTrip = new MutableLiveData<>();
    private static TripRepository mTripRepository;
    private File mFilesDir;

    public synchronized static TripRepository getInstance(Application application) {
        if (mTripRepository == null)
            mTripRepository = new TripRepository(application);
        return mTripRepository;
    }

    private TripRepository(Application application) {
        apiService = RetrofitInstance.getApiService();
        mFilesDir = application.getApplicationContext().getFilesDir();
    }

    public File getPhotoFile(Trip trip) {
        return new File(mFilesDir, trip.getPhoto());
    }

    public void allTrips(Request request, LiveData<Request> liveData) {
        Call<ArrayList<Trip>> call = apiService.allTrips();
        call.enqueue(new Callback<ArrayList<Trip>>() {
            @Override
            public void onResponse(@NotNull Call<ArrayList<Trip>> call, @NotNull Response<ArrayList<Trip>> response) {
                if(response.code() == 200) {
                    List<Trip> lst = response.body();
                    if(lst != null)
                        Collections.sort(lst);
                    new Handler().postDelayed(() -> ((MutableLiveData) liveData).setValue(request.getRequestResponse(1, lst)), 500);
                }else
                    fail("(" + response.code() + ") " + response.toString());
            }
            @Override
            public void onFailure(@NotNull Call<ArrayList<Trip>> call, @NotNull Throwable t) {
                fail(t.getMessage());
            }
            private void fail(String msg){
                ((MutableLiveData)liveData).setValue(request.getRequestResponse(-1, null));
                Log.e("ERROR TRIPPING", "ALLTRIPS " + msg);
            }
        });
        ((MutableLiveData)liveData).setValue(request);
    }

    public void insertTrip(Request request, LiveData<Request> liveData) {
        Call<Trip> call = apiService.insertTrip((Trip)request.getT());
        call.enqueue(new Callback<Trip>() {
            @Override
            public void onResponse(@NotNull Call<Trip> call, @NotNull Response<Trip> response) {
                if(response.code() == 200)
                    ((MutableLiveData)liveData).setValue(request.getRequestResponse(1, response.body()));
                else
                    fail("(" + response.code() + ") " + response.toString());
            }
            @Override
            public void onFailure(@NotNull Call<Trip> call, @NotNull Throwable t) {
                fail(t.getMessage());
            }
            private void fail(String msg){
                ((MutableLiveData)liveData).setValue(request.getRequestResponse(-1, null));
                Log.e("ERROR TRIPPING", "INSERTTRIP " + msg);
            }
        });
        ((MutableLiveData)liveData).setValue(request);
    }

    public void deleteTrip(Request request, LiveData<Request> liveData) {
        Call<Void> call = apiService.deleteTrip((Trip)request.getT());
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NotNull Call<Void> call, @NotNull Response<Void> response) {
                if(response.code() == 200)
                    ((MutableLiveData)liveData).setValue(request.getRequestResponse(1, null));
                else
                    fail("(" + response.code() + ") " + response.toString());
            }
            @Override
            public void onFailure(@NotNull Call<Void> call, @NotNull Throwable t) {
                fail(t.getMessage());
            }
            private void fail(String msg){
                ((MutableLiveData)liveData).setValue(request.getRequestResponse(-1, null));
                Log.e("ERROR TRIPPING", "DELETETRIP " + msg);
            }
        });
        ((MutableLiveData)liveData).setValue(request);
    }

    public MutableLiveData<Trip> getTripById(UUID uuid) {
        Call<ArrayList<Trip>> call = apiService.getTripById(uuid.toString());
        call.enqueue(new Callback<ArrayList<Trip>>() {
            @Override
            public void onResponse(@NotNull Call<ArrayList<Trip>> call, @NotNull Response<ArrayList<Trip>>
                    response) {
                Trip mTrip;
                if(response.body() != null)
                    mTrip = response.body().get(0);
                else
                    mTrip = null;
                mutableLiveDataTrip.setValue(mTrip);
            }
            @Override
            public void onFailure(@NotNull Call<ArrayList<Trip>> call, @NotNull Throwable t) {
                Log.e("ERROR TRIPPING", "GETTRIPBYID " + Objects.requireNonNull(t.getMessage()));
            }
        });
        return mutableLiveDataTrip;
    }
}
